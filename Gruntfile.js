'use strict';

module.exports = function (grunt) {

    // Project configuration.
    grunt.config.init({
        pkg: grunt.file.readJSON('package.json'),
        name: 'windvane',
        srcPath: 'src',
        distPath: 'build',
        apidocPath: 'api',

        clean: ['<%=distPath%>/*'],

        copy: {
        	package: {
                files: [{
                    expand: true,
                    cwd: './',
                    src: ['package.json'],
                    dest: '<%= distPath %>'
                }]
        	}
        },
        	
    	depconcat: {
    		main: {
    			files: [{
    				src: ['<%= srcPath%>/windvane.js'],
    				dest: '<%= distPath%>/windvane.debug.js'
    			}]
    		},
            demo: {
                files: [{
                    src: ['demo/demo.js'],
                    dest: '<%= distPath%>/demo.debug.js'
                }]
            },
            printlog: {
                files: [{
                    src: ['<%= srcPath%>/printlog.js'],
                    dest: '<%= distPath%>/printlog.debug.js'
                }]
            },
            hybridapi: {
                files: [{
                    src: ['<%= srcPath%>/hybridapi.js'],
                    dest: '<%= distPath%>/hybridapi.debug.js'
                }]
            }
    	},
        
		uglify: {
           main: {
               files: [{
					expand: true,
					cwd: '<%= distPath %>/',
					src: ['*.debug.js'],
					dest: '<%= distPath %>/',
					ext: '.js'
			   }]
           }
        },

        watch: {
            main: {
                files: ['<%= srcPath%>/*.js', 'demo/*.js'],
                tasks: ['clean', 'copy', 'depconcat', 'uglify']
            }
        },

        cmdwrap: {
            js: {
                files: [{
                    expand: true,
                    cwd: '<%= distPath %>',
                    src: ['windvane.js', 'hybridapi.js'],
                    dest: '<%= distPath %>',
                    ext: '.cmd.js'
                }]
            }
        },

        commonizor: {
            main: {
                files: [{
                    expand: true,
                    cwd: '<%= distPath%>',
                    src: ['<%= name%>.js'],
                    dest: '<%=distPath%>',
                    ext: '.common.js'
                }]
            }
        },

        jsdoc: {
            main : {
                src: ['<%= srcPath %>/windvane.js', 'README.md'],
                options: {
                    destination: '<%= apidocPath %>',
                    template: 'bower_components/jsdoc'
                }
            }
        }

    });

    // grunt plugins
    grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-depconcat');
    grunt.loadNpmTasks('grunt-cmdwrap');
    grunt.loadNpmTasks('grunt-jsdoc');
    grunt.loadNpmTasks('@ali/grunt-commonizor');

    // Default grunt
    grunt.registerTask('default', ['clean', 'depconcat', 'uglify', 'cmdwrap', 'commonizor', 'jsdoc', 'copy']);

};
